#include <stdlib.h>
#include <threads.h>

#include <cwav.h>
#include <cwav_internal.h>

size_t samples(wav_file *wav) {
    return wav->data->size / (wav->fmt->bit_depth / 8);
}

channel *extract_channel(wav_file *wav, size_t ch) {
    size_t chs = wav->fmt->channels;
    size_t spc = samples(wav) / chs;

    channel *channel = malloc(sizeof(channel));
    channel->size = spc;
    channel->data = malloc(samples(wav) * sizeof(float) / chs);

    for (size_t i = 0; i < spc; ++i)
        channel->data[i] = wav->data->data[i * chs + ch];

    return channel;
}

void insert_channel(wav_file *wav, channel *channel, size_t ch) {
    size_t spc = samples(wav) / wav->fmt->channels;
    for (size_t i = 0; i < spc; ++i)
        wav->data->data[i * wav->fmt->channels + ch] = channel->data[i];
}

void destroy_channel(channel *channel) {
    free(channel->data);
    free(channel);
}

int thread_wrapper(struct wrapper_args *wargs) {
        channel *channel = extract_channel(wargs->wav, wargs->ch);
        wargs->callback(channel, wargs->args);
        insert_channel(wargs->wav, channel, wargs->ch);
        destroy_channel(channel);
        destroy_wargs(wargs);
        return 0;
}

wav_file *mt_process(wav_file *wav,
                     int (*callback)(channel *channel, float *args),
                     float *args) {

    size_t chs = wav->fmt->channels;
    thrd_t ids[chs];

    for (size_t ch = 0; ch < chs; ++ch) {
        struct wrapper_args *wargs = malloc(sizeof(struct wrapper_args));
        wargs->args = args;
        wargs->callback = callback;
        wargs->ch = ch;
        wargs->wav = wav;
        thrd_create(&ids[ch], (thrd_start_t)thread_wrapper, wargs);
    }

    for (size_t ch = 0; ch < chs; ++ch) {
        thrd_join(ids[ch], NULL);
    }

    return wav;
}

void destroy_wargs(struct wrapper_args *wargs) {
    free(wargs);
}

void destroy_wav(wav_file *wav) {
    free(wav->riff);
    free(wav->fmt);
    if (wav->fact)
        free(wav->fact);
    free(wav->data->data);
    free(wav->data);
    free(wav);
}
