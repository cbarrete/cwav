#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <cwav.h>
#include <cwav_internal.h>

static int read_riff(FILE *f, wav_file *wav) {
    uint8_t id[4];
    fread(id, 4, 1, f);
    if (memcmp(id, "RIFF", 4)) {
        fprintf(stderr, "cwav: the file is not a RIFF file\n");
        return 1;
    }

    wav->riff = malloc(sizeof(struct riff_chunk));
    memcpy(wav->riff->id, "RIFF", 4);

    fread(&wav->riff->chunk_size,   4, 1, f);
    fread(wav->riff->type,          4, 1, f);

    if (memcmp(wav->riff->type, "WAVE", 4)) {
        fprintf(stderr, "cwav: the file is not a WAVE file\n");
        return 1;
    }
    return 0;
}

static void read_fmt(FILE *f, wav_file *wav) {
    wav->fmt = malloc(sizeof(struct fmt_chunk));
    memcpy(wav->fmt->id, "fmt ", 4);

    fread(&wav->fmt->size,          4, 1, f);
    fread(&wav->fmt->audio_format,  2, 1, f);
    fread(&wav->fmt->channels,      2, 1, f);
    fread(&wav->fmt->sample_rate,   4, 1, f);
    fread(&wav->fmt->byte_rate,     4, 1, f);
    fread(&wav->fmt->align,         2, 1, f);
    fread(&wav->fmt->bit_depth,     2, 1, f);
}

static void read_fact(FILE *f, wav_file *wav) {
    wav->fact = malloc(sizeof(struct fact_chunk));
    memcpy(wav->fact->id, "fact", 4);

    fread(&wav->fact->size,             4, 1, f);
    fread(&wav->fact->samples_per_ch,   4, 1, f);
}

static void read_data(FILE *f, wav_file *wav) {
    wav->data = malloc(sizeof(struct data_chunk));
    memcpy(wav->data->id, "data", 4);
    fread(&wav->data->size, 4, 1, f);

    if (wav->fmt->audio_format == IEEE_FLOAT) {
        wav->data->data = malloc(wav->data->size);
        fread(wav->data->data, wav->data->size, 1, f);
    } else if (wav->fmt->audio_format == PCM) {
        float factor = powf(2, wav->fmt->bit_depth) - 1;

        wav->data->data = calloc(samples(wav), sizeof(float));
        void *blob = calloc(1, wav->data->size);
        fread(blob, wav->data->size, 1, f);

        size_t size = wav->fmt->align / wav->fmt->channels;
        int32_t *tmp = malloc(sizeof(int32_t));

        for (int i = 0; i < samples(wav); ++i) {
            memcpy(tmp, blob+i*size, size);

            /* the sign bit is too low to be registered by an int32 */
            /* will be handled better than that of course */
            if (*tmp < factor/2)
                wav->data->data[i] = (float)*tmp / factor;
            else
                wav->data->data[i] = ((float)*tmp / factor) - 1;
        }

        free(tmp);
        free(blob);
    } else {
        fprintf(stderr, "cwav: audio format %d not supported\n",
                wav->fmt->audio_format);
    }
}

wav_file *read_metadata(char *filename) {
    FILE *f = fopen(filename, "rb");

    if (!f) {
        perror(filename);
        return NULL;
    }

    wav_file *wav = fread_metadata(f);

    fclose(f);

    return wav;
}

wav_file *fread_metadata(FILE *f) {
    wav_file *wav = malloc(sizeof(wav_file));
    wav->fact = NULL; /* there may be no fact chunk */
    wav->data = NULL; /* no data read here */

    int error = read_riff(f, wav);
    if (error)
        return NULL;

    uint8_t id[4];
    do {
        fread(id, 4, 1, f);
        if (!memcmp(id, "fmt ", 4))
            read_fmt(f, wav);
        else if (!memcmp(id, "fact ", 4))
            read_fact(f, wav);
        else if (memcmp(id, "data ", 4))
            fprintf(stderr, "cwav: unkown chunk id %4s\n", id);
    } while (memcmp(id, "data", 4));

    return wav;
}

wav_file *fread_wav(FILE *f) {
    wav_file *wav = fread_metadata(f);

    if (wav == NULL)
        return NULL;

    read_data(f, wav);
    return wav;
}

wav_file *read_wav(char *filename) {
    FILE *f = fopen(filename, "rb");

    if (!f) {
        perror(filename);
        return NULL;
    }

    wav_file *wav = fread_wav(f);

    fclose(f);

    return wav;
}

void write_data(FILE *f, wav_file *wav) {
    fwrite(wav->data, sizeof(struct data_chunk) - sizeof(wav->data->data), 1, f);
    if (wav->fmt->audio_format == PCM) {
        double factor = pow(2, wav->fmt->bit_depth) - 1;

        void *blob = malloc(wav->data->size);
        int32_t *tmp = malloc(sizeof(int32_t));

        size_t size = wav->fmt->align / wav->fmt->channels;
        for (int i = 0; i < samples(wav); ++i) {
            *tmp = wav->data->data[i] * factor;
            memcpy(blob+i*size, tmp, size);
        }

        fwrite(blob, wav->data->size, 1, f);
        free(blob);
        free(tmp);

    } else if (wav->fmt->audio_format == IEEE_FLOAT) {
        /* already float, do nothing and write */
        fwrite(wav->data->data, wav->data->size, 1, f);
    } else {
        fprintf(stderr, "cwav: audio format %d not supported",
                wav->fmt->audio_format);
    }

    fwrite(wav->data->data, wav->data->size, 1, f);
}

int fwrite_wav(wav_file *wav, FILE *f) {
    if (wav == NULL)
        return 1;

    fwrite(wav->riff, sizeof(struct riff_chunk), 1, f);
    fwrite(wav->fmt,  sizeof(struct fmt_chunk),  1, f);
    if (wav->fact)
        fwrite(wav->fact, sizeof(struct fact_chunk), 1, f);

    write_data(f, wav);
    return 0;
}

int write_wav(wav_file *wav, char *filename) {
    FILE *f = fopen(filename, "wb");
    if (!f) {
        perror(filename);
        return 1;
    }

    int error = fwrite_wav(wav, f);

    if (error) {
        return 1;
        fclose(f);
    }

    fclose(f);
    return 0;
}
