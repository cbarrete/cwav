#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#include <cwav.h>
#include <cwav_internal.h>

wav_file *gain_st(wav_file *wav, float gain) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
        wav->data->data[i] *= gain;
    return wav;
}

wav_file *full_wave_rectify_st(wav_file *wav) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
        if (wav->data->data[i] < 0)
            wav->data->data[i] *= -1;
    return wav;
}

wav_file *half_wave_rectify_st(wav_file *wav) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
        if (wav->data->data[i] < 0)
            wav->data->data[i] = 0;
    return wav;
}

wav_file *invert_phase_st(wav_file *wav) {
    if (wav == NULL)
        return wav;

    return gain_st(wav, -1);
}

wav_file *bitcrush_st(wav_file *wav, float depth) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
        wav->data->data[i] = roundf(depth * wav->data->data[i]) / depth;
    return wav;
}

/* fold */

wav_file *fold_st(wav_file *wav, float gain) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
        wav->data->data[i] = sin(gain * 2 * M_PI * wav->data->data[i]);
    return wav;
}

static int fold_channel(channel *channel, float *gain) {
    float *data = channel->data;
    for (size_t i = 0; i < channel->size; ++i)
        data[i] = sin(*gain * 2 * M_PI * data[i]);
    return 0;
}

wav_file *fold_mt(wav_file *wav, float gain) {
    if (wav == NULL)
        return wav;

    float *arg = malloc(sizeof(float));
    *arg = gain;
    mt_process(wav, fold_channel, arg);
    free(arg);
    return wav;
}

/* fractalizer */

static bool zcrossing(channel *channel, int cur) {
    float *data = channel->data;
    if ((data[cur] < 0) != (data[cur+1] < 0))
        return true;
    else
        return false;
}

static float *frac_cycle(float *buffer, int depth,
        size_t cycl_beg, size_t cycl_len) {
    size_t size = cycl_len / depth;
    float *output = malloc(sizeof(float) * size);
    for (size_t i = 0; i < size; ++i)
        output[i] = buffer[cycl_beg + depth * i];
    return output;
}

static int fractalize_channel(channel *channel, float *depth) {
    size_t cur = 0, cycl_beg = 0, zcross = 0;

    size_t idepth = (int)*depth;

    while (cur < channel->size) {
        if (cur == channel->size ||
                (zcrossing(channel, cur) && ++zcross == 2)) {
            size_t cycl_len = cur - cycl_beg;
            float *frac = frac_cycle(channel->data, idepth, cycl_beg, cycl_len);
            for (size_t i = cycl_beg; i < cur; ++i)
                if (cycl_len / idepth != 0)
                    channel->data[i] = frac[(i - cycl_beg) % (cycl_len / idepth)];
            cycl_beg = cur;
            zcross = 0;
        }
        ++cur;
    }
    return 0;
}

wav_file *fractalize_mt(wav_file *wav, int depth) {
    if (wav == NULL)
        return wav;

    float *arg = malloc(sizeof(float));
    *arg = roundf(depth);
    mt_process(wav, fractalize_channel, arg);
    free(arg);
    return wav;
}

/* arctan waveshaper */

wav_file *waveshape_st(wav_file *wav, float gain) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
            wav->data->data[i] = atanf(gain * wav->data->data[i]);

    return wav;
}

/* waveshaper with tension */

wav_file *waveshape_tension_st(wav_file *wav, float tension) {
    if (wav == NULL)
        return wav;

    for (size_t i = 0; i < samples(wav); ++i)
            wav->data->data[i] = 1 - pow(1 - wav->data->data[i], tension);

    return wav;
}
