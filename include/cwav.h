#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

struct riff_chunk {
    uint8_t  id[4];
    uint32_t chunk_size;
    uint8_t  type[4];
};

struct fmt_chunk {
    uint8_t  id[4];
    uint32_t size;
    uint16_t audio_format;
    uint16_t channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t align;
    uint16_t bit_depth;
};

struct fact_chunk {
    uint8_t  id[4];
    uint32_t size;
    uint32_t samples_per_ch;
};

struct data_chunk {
    uint8_t  id[4];
    uint32_t size;
    float    *data;
};

typedef struct {
    struct riff_chunk *riff;
    struct fmt_chunk  *fmt;
    struct fact_chunk *fact;
    struct data_chunk *data;
} wav_file;

enum audio_format {
    PCM = 1,
    IEEE_FLOAT = 3
};

typedef struct {
    size_t size;
    float *data;
} channel;

/* io.c */
wav_file *read_wav(char *filename);
wav_file *fread_wav(FILE *f);
wav_file *read_metadata(char *filename);
wav_file *fread_metadata(FILE *f);
int write_wav(wav_file *wav, char *filename);
int fwrite_wav(wav_file *wav, FILE *f);

/* effects.c */
wav_file *gain_st(wav_file *wav, float gain);
wav_file *full_wave_rectify_st(wav_file *wav);
wav_file *half_wave_rectify_st(wav_file *wav);
wav_file *invert_phase_st(wav_file *wav);
wav_file *bitcrush_st(wav_file *wav, float depth);
wav_file *fold_st(wav_file *wav, float gain);
wav_file *fold_mt(wav_file *wav, float gain);
wav_file *fractalize_mt(wav_file *wav, int depth);
wav_file *mt_process(wav_file *wav,
        int (*callback)(channel *channel, float *args),
        float *args);
wav_file *waveshape_st(wav_file *wav, float gain);
wav_file *waveshape_tension_st(wav_file *wav, float tension);

/* tools.c */
size_t samples(wav_file *wav);
void destroy_wav(wav_file *wav);
