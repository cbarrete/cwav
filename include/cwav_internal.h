#pragma once

#include <cwav.h>

struct wrapper_args {
    float *args; /* argument to the callback */
    int (*callback)(channel *channel, float *args); /* the callback itself */
    size_t ch; /* channel to process */
    wav_file *wav; /* wav to write back to */
};

/* tools.c */
channel *extract_channel(wav_file *wav, size_t ch);
void insert_channel(wav_file *wav, channel *channel, size_t ch);
void destroy_channel(channel *channel);

int thread_wrapper(struct wrapper_args *wargs);
void destroy_wargs(struct wrapper_args *wargs);
