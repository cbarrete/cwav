# Quickstart

- Compile the library with `make`
- Include the `cwav.h` header
- Use the `read_wav` and `write_wav` functions for IO
- Compile your program with the following options:
    - -L /path/to/lib/directory
    - -I /path/to/include/directory
    - -lcwav
    - -lm
    - -pthread

# Features

- Lightweight
- Easy to use
- Multithreaded
- Portable thanks to C11
- Can multi-thread user-defined callbacks to process audio
- Daisy-chainable API

# Caveats

- Although the library has no platform-specific code, the build system may have
  to be tweaked on non-Linux systems
- The user-defined callbacks are applied per-channel and cannot communicate with
  other channels for now
- The API is not stable yet and is likely to change in the near future

# Example programs

On Unix-like systems, these example programs can be compiled with:

```bash
gcc main.c -L lib -I include -lcwav -lm -pthread
```

## Gain

This minimal program takes two filenames as arguments and outputs a louder
version of the first one in the second one:

```c
#include <cwav.h>

int main(int argc, char *argv[]) {
    wav_file *wav = read_wav(argv[1]);
    wav = gain(wav, 2);
    write_wav(wav, argv[2]);
    return 0;
}
```

## Custom callback

This program uses a user-defined callback to process the audio file passed as an
argument. The callback takes a `channel` (defined in `cwav.h`) and a pointer to
float parameters, although no paramters are used here:

```c
#include <cwav.h>
#include <stdlib.h>

int my_callback(channel *channel, float *arg) {
    for (size_t i = 0; i < channel->size; ++i)
        if (channel->data[i] < 0)
            channel->data[i] *= -1;
    return 0;
}

int main(int argc, char *argv[]) {
    wav_file *wav = read_wav(argv[1]);
    mt_process(wav, my_callback, NULL);
    write_wav(wav, argv[2]);
    return 0;
}
```

# License

This library is licensed under the GPLv3

See COPYING for more information
