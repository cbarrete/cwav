OBJ = io.o tools.o effects.o
STATIC_LIB = lib/libcwav.a
SHARED_LIB = lib/libcwav.so
OPTS = -Wall -I include -O2 -fPIC

%.o: src/%.c
	gcc $(OPTS) -c $<

default: static

static: $(OBJ)
	mkdir -p lib
	ar rc $(STATIC_LIB) $(OBJ)
	ranlib $(STATIC_LIB)

shared: $(OBJ)
	mkdir -p lib
	gcc -shared -o $(SHARED_LIB) $(OBJ)

clean:
	rm -f *.o
